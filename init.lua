local lpeg = require 'lpeg'
local lanes = require 'lanes'.configure()

local M = {}

M.vm = {}
M.vm.__index = M.vm

function M.new()
   return setmetatable(state, M.vm)
end

function M.vm:compile(src)
end

function M.vm:call(name)
end

if debug and debug.getinfo and debug.getinfo().what == 'main' then
   --do stuff
end

return M
