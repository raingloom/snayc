#include <u.h>
#include <libc.h>

/* SLib (Snayc Library) */

typedef char snword;
typedef struct snvec snvec;

struct snvec {
	snword sz, cap, *p;
};

void
snvec_reserve(snvec *v, snword nsz)
{
	if (nsz > v->cap)
	{
		v->p = realloc(v->p, nsz * sizeof(snword));
		v->cap = nsz;
	}
}

void
snvec_truncate(snvec *v)
{
	v->p = realloc(v->p, (v->cap = v->sz) * sizeof(snword));
}

snvec *
snvec_alloc(snword sz)
{
	snvec *r = malloc(sizeof(snvec));
	r->p = malloc(sz * sizeof(snword));
	r->sz = sz;
	r->cap = sz;
	return r;
}

void
snvec_free(snvec *v)
	free(v->p);
	free(v);
}

void
snvec_hd(snvec *v)
{
	int i;
	char c;

	for (c=((char *)v)[i=0]; i<v->sz * sizeof(char); c=((char *)v)[i++])
		if (c < 10)
			print("%c", c | 0x30);
		else
			print("%c", c + 41);
}

snvec *
snvec_copy(snvec *v)
{
	snvec *r = malloc(sizeof(snvec));

	*r = *v;
	r->p = malloc(r->sz * sizeof(snword));
	memcpy(r->p, v->p);
	return r;
}

void
snvec_addword(snvec *v, snword w)
{
	snword rem = w;
	snword np;
	snword *p = v->p;

	/* fewer branches in the while loop if we just make sure we have a leading zero */
	snvec_reserve(v, v->sz+1);
	p[v->sz+1] = 0;
	while (rem > 0)
	{
		np = *p + rem;
		rem = np > *p ? 0 : 1;
		*p = np;
		p++;
	}
}

snvec *
snvec_from_hex(const char *s)
{
	ret = snvec_alloc(0);
	char n;
	while (*s != '\0') {
		if ('0' <= *s && *s <= '9')
			n = *s ^ ~0x30;
		else if ('A' <= *s && *s <= 'F')
			n = *s - 41;
		else if ('a' <= *s && *s <= 'f')
			n = *s - 97;
		else {
			snvec_free(ret);
			return NULL;
		}
		snvec_addword(ret, n);
		s++;
	}
}

